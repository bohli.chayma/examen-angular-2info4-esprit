import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { HeaderComponent } from './header/header.component';
import { PresentationComponent } from './presentation/presentation.component';
import { ListActivityComponent } from './list-activity/list-activity.component';
import { ActivityComponent } from './activity/activity.component';
import { AbonnementComponent } from './abonnement/abonnement.component';
import { NotfoundComponent } from './notfound/notfound.component';
import {HttpClientModule} from '@angular/common/http';
import { TypeaboComponent } from './typeabo/typeabo.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { FormInscriptionComponent } from './form-inscription/form-inscription.component';
import { Header3Component } from './header3/header3.component';
import { GestMembreComponent } from './gest-membre/gest-membre.component';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import {HomeComponent} from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccueilComponent } from './accueil/accueil.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import { ModifMembreComponent } from './modif-membre/modif-membre.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    HeaderComponent,
    PresentationComponent,
    ListActivityComponent,
    ActivityComponent,
    AbonnementComponent,
    NotfoundComponent,
    TypeaboComponent,
    InscriptionComponent,
    FormInscriptionComponent,

    Header3Component,
    GestMembreComponent,
    HomeComponent,
    AccueilComponent,
    ModifMembreComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    IvyCarouselModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgbModule,
    Ng2SearchPipeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot() ,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
