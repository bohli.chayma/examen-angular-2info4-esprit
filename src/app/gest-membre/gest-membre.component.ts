import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Membre} from '../Model/membre';
import {MembreService} from '../serivce/membre.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {NotificationService} from '../serivce/notification.service';

@Component({
  selector: 'app-gest-membre',
  templateUrl: './gest-membre.component.html',
  styleUrls: ['./gest-membre.component.css']
})
export class GestMembreComponent implements OnInit {
  liste: Membre[];
  @Output() eventDelete = new EventEmitter<Membre>();
  @Input() membre: Membre;
  display = true;
  searchText: string;
  constructor(private service: MembreService, private router: Router,
              private toastr: NotificationService) { }

  ngOnInit(): void {
    this.service.getAll().subscribe((data: Membre[]) =>
      this.liste = data);
  }


  delete(m : Membre){
    let i = this.liste.indexOf(m);

    this.service.deleteMembre(m.id).subscribe(data  => {

      this.liste.splice(i, 1);

      this.toastr.showSuccess("sup avec success");

    });
  }
  setDisplay(){
    this.display = false;
  }
  modifRoute(id){
    this.router.navigate(['/membre', id]);

  }


}


