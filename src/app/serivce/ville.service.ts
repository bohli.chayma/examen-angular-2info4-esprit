import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Ville} from '../Model/ville';

@Injectable({
  providedIn: 'root'
})
export class VilleService {
  url = 'http://localhost:3000/ville';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Ville[]>(this.url);
  }
}
