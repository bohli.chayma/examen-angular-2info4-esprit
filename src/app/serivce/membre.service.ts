import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Membre} from '../Model/membre';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MembreService {

  url = 'http://localhost:3000/membre/';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Membre[]>(this.url);
  }
  addMembre(membre: Membre) {
    return this.http.post(this.url, membre);
  }

  deleteMembre(id: number) {
    return this.http.delete(this.url + id);
  }
  updateMembre(id: number, membre: Membre) {
    return this.http.put(this.url + id, membre);
  }
  getMembreById(id: number) {
    console.log('da', id);
    return this.http.get<Membre>(this.url + id);
    console.log('da', id);

  }

}
