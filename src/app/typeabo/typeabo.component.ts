import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeAbo} from '../Model/TypeAbo';
import {Tarif} from '../Model/tarif';
import {TypeAboService} from '../serivce/type-abo.service';
import {Activity} from '../Model/act';
import {Router} from '@angular/router';

@Component({
  selector: 'app-typeabo',
  templateUrl: './typeabo.component.html',
  styleUrls: ['./typeabo.component.css']
})
export class TypeaboComponent implements OnInit {
  @Input() TypeAbo: TypeAbo;
  list: TypeAbo[];

  @Output() saveEvent = new EventEmitter<TypeAbo>();
  constructor(private router: Router) { }


  ngOnInit(): void {
  }

  sendNotifParent() {
    this.saveEvent.emit(this.TypeAbo);
    this.router.navigateByUrl('/inscription');
  }
}
