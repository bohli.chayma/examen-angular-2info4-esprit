import {Component, Input, OnInit} from '@angular/core';
import {Activity} from '../Model/act';
import {ActivityService} from '../serivce/activity.service';

@Component({
  selector: 'app-list-activity',
  templateUrl: './list-activity.component.html',
  styleUrls: ['./list-activity.component.css']
})
export class ListActivityComponent implements OnInit {
  list: Activity[];
constructor(private service: ActivityService) {
}
  ngOnInit(): void {
    this.service.getAll().subscribe((data: Activity[]) =>
      this.list = data);
  }

}
