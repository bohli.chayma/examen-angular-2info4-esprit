import { Injectable } from '@angular/core';
import {Utilisateur} from '../Model/utilisateur';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = 'http://localhost:3000/login';
  constructor(private http: HttpClient) { }

  public seConnecter(userInfo: Utilisateur){
    localStorage.setItem('ACCESS_TOKEN', 'access_token');
  }
  public estConnecte(){
    return localStorage.getItem('ACCESS_TOKEN') !== null;

  }
  public deconnecter(){
    localStorage.removeItem('ACCESS_TOKEN');
  }
  auth(login: string, password: string){
    let params = new HttpParams();
    params = params.append('login', login);
    params = params.append('password', password);
    return this.http.get(this.url, {params: params});
  }
}
