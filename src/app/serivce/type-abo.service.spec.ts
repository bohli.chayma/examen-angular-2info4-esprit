import { TestBed } from '@angular/core/testing';

import { TypeAboService } from './type-abo.service';

describe('TypeAboService', () => {
  let service: TypeAboService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeAboService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
