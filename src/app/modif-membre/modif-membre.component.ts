import { Component, OnInit } from '@angular/core';
import {Membre} from '../Model/membre';
import {MembreService} from '../serivce/membre.service';
import {ActivatedRoute} from '@angular/router';
import {Ville} from '../Model/ville';
import {TypeAbo} from '../Model/TypeAbo';
import {VilleService} from '../serivce/ville.service';
import {TypeAboService} from '../serivce/type-abo.service';
import {NotificationService} from '../serivce/notification.service';

@Component({
  selector: 'app-modif-membre',
  templateUrl: './modif-membre.component.html',
  styleUrls: ['./modif-membre.component.css']
})
export class ModifMembreComponent implements OnInit {
  membreItem: Membre;
  list: Ville[];
  liste: TypeAbo[];
  membre: Membre;
  constructor(private service: VilleService, private services: TypeAboService,
              private serviceMembre: MembreService, private route: ActivatedRoute, private toastr: NotificationService) { }

  ngOnInit(): void {
    this.serviceMembre.getMembreById(this.route.snapshot.params.id).subscribe(
      data => this.ajouMembre(data)
    );
    console.log('membre', this.membreItem);
    this.membre = new Membre();
}


  modif(){
    this.membre = this.membreItem;
    this.serviceMembre.updateMembre(this.membreItem.id, this.membre).subscribe();
    this.toastr.showSuccess("Modification avec success");
  }

  ajouMembre(data){
    this.membreItem = data;
    console.log('membre', this.membreItem);
  }
}
