import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from  '@angular/router';
import {AuthService} from '../serivce/auth.service';
import {Utilisateur} from '../Model/utilisateur';



@Component({
  selector: 'app-connexion',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isSubmitted  =  false;

  constructor(private authService: AuthService,
              private router: Router, private formBuilder: FormBuilder ) { }
  ngOnInit() {
    this.loginForm  =  this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  get formControls() { return this.loginForm.controls; }

  seConnecter(){
    console.log(this.loginForm.value);
    this.isSubmitted = true;
    if(this.loginForm.invalid){
      return ;
    }
    this.authService.seConnecter(this.loginForm.value);
    this.router.navigateByUrl('/admin');

  }
  authentification(){
    this.authService.auth(this.loginForm.value, this.loginForm.value).subscribe(
      (data: Utilisateur) => {
        sessionStorage.setItem('login', (data[0].login));
        console.log('admin', data[0].login);
        window.location.reload();
      },
      (error) => console.log('erreur', error)
    );
  }
}
