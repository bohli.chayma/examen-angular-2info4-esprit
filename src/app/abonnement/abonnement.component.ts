import {Component, Input, OnInit} from '@angular/core';
import {Activity} from '../Model/act';
import {TypeAbo} from '../Model/TypeAbo';
import {ActivityService} from '../serivce/activity.service';
import {TypeAboService} from '../serivce/type-abo.service';
import {Tarif} from '../Model/tarif';
import {TarifService} from '../serivce/tarif.service';

@Component({
  selector: 'app-abonnement',
  templateUrl: './abonnement.component.html',
  styleUrls: ['./abonnement.component.css']
})
export class AbonnementComponent implements OnInit {
  list: TypeAbo[];
  constructor(private services: TypeAboService) { }
  ngOnInit(): void {
    this.services.getAll().subscribe((data: TypeAbo[]) =>
      this.list = data);
  }

}
