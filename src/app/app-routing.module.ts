import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {AdminComponent} from './admin/admin.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {AbonnementComponent} from './abonnement/abonnement.component';
import {PresentationComponent} from './presentation/presentation.component';
import {ListActivityComponent} from './list-activity/list-activity.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {InscriptionComponent} from './inscription/inscription.component';
import {GestMembreComponent} from './gest-membre/gest-membre.component';
import {ModifMembreComponent} from './modif-membre/modif-membre.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  { path: 'activites', component: ListActivityComponent },
  { path: 'connexion', component: LoginComponent },
  { path: 'abonnement', component: AbonnementComponent },
  { path: 'presentation', component: PresentationComponent },
  { path: 'inscription', component: InscriptionComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'gestMembre', component: GestMembreComponent },
  { path: 'membre/:id', component: ModifMembreComponent },
  { path: 'admin', component: GestMembreComponent },
  {path: '**', component: NotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
