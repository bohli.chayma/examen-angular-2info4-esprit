import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestMembreComponent } from './gest-membre.component';

describe('GestMembreComponent', () => {
  let component: GestMembreComponent;
  let fixture: ComponentFixture<GestMembreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestMembreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestMembreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
