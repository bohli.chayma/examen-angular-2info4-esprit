import { Injectable } from '@angular/core';
import {Tarif} from '../Model/tarif';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TarifService {
  url = 'http://localhost:3000/tarif';

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get<Tarif[]>(this.url);
  }
}
