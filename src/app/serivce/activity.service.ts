import { Injectable } from '@angular/core';
import {Activity} from '../Model/act';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  url = 'http://localhost:3000/activity';

  constructor(private http: HttpClient) {
  }
  getAll() {
    return this.http.get<Activity[]>(this.url);
  }

}
