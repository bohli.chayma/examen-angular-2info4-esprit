import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {TypeAbo} from '../Model/TypeAbo';

@Injectable({
  providedIn: 'root'
})
export class TypeAboService {
  url = 'http://localhost:3000/typeAbo';

  constructor(private http: HttpClient) {
  }
  getAll() {
    return this.http.get<TypeAbo[]>(this.url);
  }
  addTypeAbo(typeAbo: TypeAbo) {
    return this.http.post(this.url, typeAbo);
  }
  deleteTypeAbo(id: number) {
    return this.http.delete(this.url + id);
  }
  updateTypeAbo(id: number, typeAbo: TypeAbo) {
    return this.http.put(this.url + id, typeAbo);
  }
  serachTypeAbo(id: number){
    return this.http.get<TypeAbo>(this.url+ "/" + id);
  }
}
