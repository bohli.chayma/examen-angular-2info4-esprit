import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Ville} from '../Model/ville';

import {VilleService} from '../serivce/ville.service';
import {TypeAbo} from '../Model/TypeAbo';
import {TypeAboService} from '../serivce/type-abo.service';
import {Membre} from '../Model/membre';
import {MembreService} from '../serivce/membre.service';
import {NotificationService} from '../serivce/notification.service';


@Component({
  selector: 'app-form-inscription',
  templateUrl: './form-inscription.component.html',
  styleUrls: ['./form-inscription.component.css']
})
export class FormInscriptionComponent implements OnInit {
  list: Ville[];
  liste: TypeAbo[];
  @Input() ville: Ville;
  @Input() TypeAbo: TypeAbo;
  listeP;
  membre: Membre;
  listeM: Membre[];
  constructor(private service: VilleService,private services: TypeAboService,private membreService: MembreService,private toastr: NotificationService)
  { }

  ngOnInit(): void {
    this.membre = new Membre();
    this.service.getAll().subscribe((data: Ville[]) =>
      this.list = data);
    this.services.getAll().subscribe((data: TypeAbo[]) =>
      this.liste = data);

  }


  onOptionsSelected(e){
  console.log("onOptionsSelected", e);
    this.services.serachTypeAbo(e).subscribe(data => {
      console.log(this.listeP = data.prix);
    });

  }

  save() {
    this.membreService.addMembre(this.membre).subscribe(data =>
      console.log(data));
    this.toastr.showSuccess("ajouté avec success");
  }
}
