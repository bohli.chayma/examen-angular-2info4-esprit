export class Membre {
  id: number;
  name: string;
  prenom: string;
  dateNaisc: Date;
  NumTel: number;
  email: string;
  adresseP: string;
  idPaiement: string;
  codeVille: number;
  idTypeAbo: number;
}
