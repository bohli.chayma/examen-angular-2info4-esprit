export class Activity {
  id: number;
  name: string;
  description: string;
  img: string;
  statut: string;
}
