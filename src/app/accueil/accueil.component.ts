import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  images = [1, 2, 3].map((n) => `assets/images/home/${n}.jfif`);


  constructor() { }

  ngOnInit(): void {
  }

}
