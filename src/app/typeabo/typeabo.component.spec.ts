import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeaboComponent } from './typeabo.component';

describe('TypeaboComponent', () => {
  let component: TypeaboComponent;
  let fixture: ComponentFixture<TypeaboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeaboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeaboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
